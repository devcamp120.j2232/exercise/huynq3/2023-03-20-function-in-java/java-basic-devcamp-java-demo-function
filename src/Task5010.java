public class Task5010 {
    public static void main(String[] args) throws Exception {
        Task5010 foo= new Task5010();
        int total=foo.sumNumbersV1();

        total=Task5010.sumNumbersV1a();
        System.out.println("Hello, World!. Total 1 to 100= "+ total);
        System.out.println("Static. Total 1 to 100= "+ total);

        int [] number1 =  {1,5,10};
        int number2[] =  {1,2,3,5,7,9};
        int totalArr1, totalArr2;
        totalArr1= foo.sumNumbersV2(number1);
        totalArr2= foo.sumNumbersV2(number2);
        System.out.println("Tổng Array 1= " +totalArr1);
        System.out.println("Tổng Array 2= " +totalArr2);

        Task5010.printHello(24);
        Task5010.printHello(99);
    }
    public int sumNumbersV1() {
       int  sum=0;
       for (int i=1; i<=100; i++){
        sum= sum+i;
       }
        return sum;
    }
    public static int sumNumbersV1a() {
        int  sum=0;
        for (int i=1; i<=100; i++){
         sum= sum+i;
        }
         return sum;
     }
    public int sumNumbersV2(int[] numbers){
        int total=0;
        for (int i=0; i<numbers.length;i++){
            total= total+numbers[i];
        }
        return total;
    }
    public static void printHello(int number){
        if (number%2==1){
            System.out.println(number+ ": Đây là số lẻ");
        }
        else { System.out.println(number+ ": Đây là số chẳn");}
    }
}
